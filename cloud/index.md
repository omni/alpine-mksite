<!-- begin cloud -->
<script type="text/javascript" src="/js/cloud.js"></script>
<div class="pure-g">

    <div class="pure-u-1">
        <div class="l-box">
            <h1>Cloud Images</h1>
            <p>
                Currently, only AWS cloud images are available; other major
                cloud providers and "NoCloud" will be supported in the future.
            </p>
            <p>
                Image variants for two instance bootstrap systems are available --
                <a href="https://gitlab.alpinelinux.org/alpine/cloud/tiny-cloud">
                Tiny Cloud</a> and <a href="https://cloud-init.io">cloud-init</a>.
            </p>
            <p>
                The login user for the images is <b>alpine</b>. SSH keys for
                this user will be installed from the instance metadata service.
            </p>
            <p>
                <i>These images are built and published using the <a
                href="https://gitlab.alpinelinux.org/alpine/cloud/alpine-cloud-images">
                Alpine Linux Cloud Images Builder</a>.  To report problems or
                request features, please open an <a
                href="https://gitlab.alpinelinux.org/alpine/cloud/alpine-cloud-images/-/issues">
                issue</a> with detailed information.</i>
            </p>
            <form class="pure-form pure-form-stacked">
                <fieldset>
                    <div class="pure-g">
                        <div class="pure-u-1 pure-u-md-1-4">
                            <label for="cloud"><b>Cloud Provider</b></label>
                            <select id="f_cloud" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.clouds}}
                                <option value="{{cloud}}">{{cloud_name}}</option>
                                {{/cloud/releases.filters.clouds}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-4">
                            <label for="region"><b>Launch Region</b></label>
                            <select id="f_region" class="filter" onchange="filter(this.id)">
                                <!-- NOTE: not until downloads are ready
                                <option value="">(none)</option>
                                -->
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.regions}}
                                <option value="{{region}}" id="f_{{region}}"
                                    class="f_cloud {{#clouds}}v_{{cloud}}{{/clouds}}">{{region}}</option>
                                {{/cloud/releases.filters.regions}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="release"><b>Release</b></label>
                            <select id="f_release" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.versions}}
                                <option value="{{release}}">{{release}}</option>
                                {{/cloud/releases.versions}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="arch"><b>Arch</b></label>
                            <select id="f_arch" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.archs}}
                                <option value="{{arch}}">{{arch_name}}</option>
                                {{/cloud/releases.filters.archs}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="firmware"><b>Firmware</b></label>
                            <select id="f_firmware" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.firmwares}}
                                <option value="{{firmware}}">{{firmware_name}}</option>
                                {{/cloud/releases.filters.firmwares}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="bootstrap"><b>Bootstrap</b></label>
                            <select id="f_bootstrap" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.bootstraps}}
                                <option value="{{bootstrap}}">{{bootstrap_name}}</option>
                                {{/cloud/releases.filters.bootstraps}}
                            </select>
                        </div>
                    </div>
                </fieldset>
            </form>
            <br/>
        </div>
    </div>

    {{#cloud/releases.versions}}
    <div id="f_{{release}}" class="pure-u-1 pure-u-md-1-2 f_release v_{{release}}">
        <div class="cloud download">
            <h2 title="End of Life: {{end_of_life}}">{{release}}</h2>
            {{#images}}
            <div id="f_{{image_name}}" class="f_arch v_{{arch}} f_firmware v_{{firmware}} f_bootstrap v_{{bootstrap}}">
                <table class="pure-table pure-table-bordered">
                    <thead>
                        <tr><td colspan="3" align="center">
                            <b title="Published: {{published}}"">{{image_name}}</b>
                            <!-- NOTE: downloads aren't ready yet
                            <hr>
                            <table class="pure-table pure-table-horizontal">
                                {{#downloads}}
                                <tr id="f_{{image_name}}_{{cloud}}" class="f_cloud v_{{cloud}}">
                                    <td align="center"><a href="{{{image_url}}}.{{{image_format}}}" class="green-button">
                                        <i class="fa fa-download"></i>&nbsp;{{cloud}}
                                    </a></td>
                                    <td align="center">
                                        <a class="pure-button checksums" href="{{{image_url}}}.asc"><i class="fa fa-lock"></i>&nbsp;GPG</a>
                                        <a class="pure-button checksums" href="{{{image_url}}}.yaml"><i class="fa fa-info-circle"></i>&nbsp;meta</a>
                                    </td>
                                </tr>
                                {{/downloads}}
                            </table>
                            -->
                        </td></tr>
                    </thead>
                    <tbody>
                        {{#regions}}
                        <tr id="f_{{image_name}}_{{cloud}}_{{region}}" class="f_cloud v_{{cloud}} f_region v_{{region}}">
                            <td align="center">{{cloud}}</td>
                            <td>{{region}}</td>
                            <td align="center">
                                <a class="pure-button checksums" href="{{{region_url}}}"><i class="fa fa-info-circle"></i>&nbsp;Image</a>
                                <a class="pure-button checksums" href="{{{launch_url}}}"><i class="fa fa-rocket"></i>&nbsp;Launch</a>
                            </td>
                        </tr>
                        {{/regions}}
                        <tr><td colspan="3" align="center"></td></tr>
                    </tbody>
                </table>
            </div>
            {{/images}}
        </div>
    </div>
    {{/cloud/releases.versions}}

</div>
<!-- end cloud -->
