---
title: 'Alpine 3.19.0 released'
date: 2023-12-07
---

Alpine Linux 3.19.0 Released
============================

We are pleased to announce the release of Alpine Linux 3.19.0, the first in
the v3.19 stable series.

<a name="highlights">Highlights</a>
----------

* Linux kernel [6.6](https://kernelnewbies.org/Linux_6.6)
* GCC [13.2](https://gcc.gnu.org/gcc-13/changes.html)
* Perl [5.38](https://perldoc.perl.org/perl5380delta)
* LLVM [17](https://releases.llvm.org/17.0.1/docs/ReleaseNotes.html)
* Xen [4.18](https://xenproject.org/2023/11/20/xen-project-releases-version-4-18-with-new-security-performance-and-architecture-enhancements-for-ai-ml-applications/)
* PostgreSQL [16](https://www.postgresql.org/about/news/postgresql-16-released-2715/)
* Node.js (lts) [20.10](https://nodejs.org/en/blog/release/v20.10.0)
* Ceph [18.2](https://ceph.io/en/news/blog/2023/v18-2-0-reef-released/)
* GNOME [45](https://release.gnome.org/45/)
* Go [1.21](https://go.dev/blog/go1.21)
* KDE Applications [23.08](https://kde.org/announcements/gear/23.08.0/) / KDE Frameworks [5.112](https://kde.org/announcements/frameworks/5/5.112.0/)
* OpenJDK [21](https://openjdk.org/projects/jdk/21/)
* PHP [8.3](https://www.php.net/releases/8.3/en.php)
* Rust [1.72](https://blog.rust-lang.org/2023/08/24/Rust-1.72.0.html)

<a name="significant_changes">Significant changes</a>
-------------------

Support for Raspberry Pi 5 was added.


<a name="upgrade_notes">Upgrade notes</a>
-------------

As always, make sure to use `apk upgrade --available` when switching between
  major versions.

- `openrc` has
  [removed](https://github.com/OpenRC/openrc/commit/65b13eb86faba812aaef94623b84dfc3bdeca6c7)
  the deprecated `/sbin/rc` binary. Make sure your `/etc/inittab` uses
  `/sbin/openrc`.
- `iptables-nft` is now the
  [default](https://gitlab.alpinelinux.org/alpine/aports/-/commit/f87a191922955bcf5c5f3fc66a425263a4588d48)
  iptables backend.
- `linux-rpi4` and `linux-rpi2` kernels have been replaced by a single
  `linux-rpi`
- `yggdrasil` was upgraded to
  [0.5](https://github.com/yggdrasil-network/yggdrasil-go/releases/tag/v0.5.0)
  and the new routing scheme is
  [incompatible](https://yggdrasil-network.github.io/2023/10/22/upcoming-v05-release.html)
  with previous versions.
- Python's package directory is now
  [marked](https://gitlab.alpinelinux.org/alpine/infra/aports/-/commit/6c2a300f6787544f3194e7f6cc19058baf9b7419)
  as [externally
  managed](https://packaging.python.org/en/latest/specifications/externally-managed-environments/),
  which means that `pip` can no longer install to system directory which is
  managed by `apk`. Users may use [pipx](https://pipx.pypa.io/stable/) instead.


<a name="changes">Changes</a>
-------

The full list of changes can be found in the [wiki][7],  [git log][8] and [bug tracker][9].

<a name="credits">Credits</a>
-------

Thanks to everyone sending patches, bug reports, new and updated aports,
and to everyone helping with writing documentation, maintaining the
infrastructure, or contributing in any other way!

Thanks to [GIGABYTE][1], [Linode][2], [Fastly][3], [IBM][4], [Equinix Metal][5],
[vpsFree][6] and [ungleich][10] for providing us with hardware and hosting.

[1]: https://www.gigabyte.com/
[2]: https://linode.com
[3]: https://www.fastly.com/
[4]: https://ibm.com/
[5]: https://www.equinix.com/
[6]: https://vpsfree.org
[7]: https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.19.0
[8]: https://git.alpinelinux.org/cgit/aports/log/?h=v3.19.0
[9]: https://gitlab.alpinelinux.org/alpine/aports/issues?scope=all&utf8=%E2%9C%93&state=closed&milestone_title=3.19.0
[10]: https://ungleich.ch/

### aports Commit Contributors

<pre>
6543
Adam Bruce
Adam Jensen
Adam Saponara
Adam Thiede
Ado
Adrián Arroyo Calle
Affe Null
Aiden Grossman
Aleks Bunin
Alex
Alex Denes
Alex McGrath
Alex Sivchev
Alexander Birkner
Alexey Minnekhanov
Alexey Yerin
Alistair Francis
Alois Klink
Andrei Jiroh Halili
Andrei Zavada
Andrej Kolchin
Andres Almiray
André Klitzing
Andy Hawkins
Andy Li
Andy Postnikov
Anjandev Momi
Anon Anon
Antoine Martin
Anton Bambura
Antoni Aloy Torrens
Ariadne Conill
Arnav Singh
Assaf Inbal
Aydin Mercan
Ayush Agarwal
Bader Zaidan
Bart Ribbers
Ben Westover
Benoit Masson
Bernd Rothert
Bobby Hamblin
Bryant Mairs
Bryce Vandegrift
BugFest
Caleb Connolly
Carlo Landmeter
Celeste
Chaiwat Suttipongsakul
Clayton Craft
Coco Liliace
Conrad Hoffmann
Cowington Post
Craig Comstock
Curt Tilmes
Damian Kurek
Daniel Fancsali
Daniel Néri
Dave Henderson
David Demelier
David Florness
David Heidelberg
Dekedro
Dennis Przytarski
Dermot Bradley
Dhruvin Gandhi
Dmitry Zakharchenko
Dominika Liberda
Dominique Martinet
Drew DeVault
Duncan Bellamy
Dylan Van Assche
Díaz Urbaneja Víctor Diego Alejandro (Sodomon)
Edd Salkield
Elly Fong-Jones
Eloi Torrents
Evan Johsnton
Even Rouault
Fabricio Silva
Faustin Lammler
Fiona Klute
Firas Khalil Khana
Florian Schwarzmeier
FollieHiyuki
Forza
Francesco Colista
Francesco Palumbo
Frank Oltmanns
Fxzx mic
Gabor Csardi
Gabriel Arakaki Giovanini
Galen Abell
Gavin Henry
Gennady Feldman
GitHub Action
Glenn Strauss
Grigory Kirillov
Guilherme Macedo
Guillaume Quintard
Guy Godfroy
Gábor Csárdi
Haelwenn (lanodan) Monnier
Hakan Erduman
Henrik Riomar
Hoang Nguyen
Holger Jaekel
Hristiyan Ivanov
Hugo Osvaldo Barrera
Hugo Rodrigues
Hugo Wang
Hygna
IP2Location
Iskren Chernev
Ivan Popovych
Iztok Fister Jr
Iztok Fister Jr.
J0WI
Jake Buchholz Göktürk
Jakob Hauser
Jakob Meier
Jakub Jirutka
Jakub Panek
Jan Samek
Janik Besendorf
Jason Swank
Jean-Christophe Amiel
Jeff Dickey
Jingyun Hua
Jiri Kastner
Jo Coscia
John Anthony
John Gebbie
John Vogel
Jonas
Jonathan Schleifer
Jordan Christiansen
Josef Vybíhal
Joshua Murphy
Julie Koubova
JuniorJPDJ
Justin
Justinas Grigas
KAA the Wise
Kaarle Ritvanen
Kaspar Schleiser
Kevin Daudt
Khem Raj
Klaus Frank
Klemens Nanni
Konstantin Kulikov
Krassy Boykinov
Krystian Chachuła
Laszlo Gombos
Lauren N. Liberda
Laurent Bercot
Lennart Jablonka
Leon Marz
Leon ROUX
Leonardo Arena
Lindsay Zhou
Luca Weiss
Lucidiot
M Hickford
Maarten van Gompel
Magnus Sandin
Marian Buschsieweke
Mark Hills
Markus Göllnitz
Martijn Braam
Matthew T Hoare
Matthias Ahouansou
Mauricio Sandt
Michael M
Michael Pirogov
Michael Truog
Michal Jirku
Michal Tvrznik
Michał Adamski
Michał Polański
Michał Szmidt
Mike Crute
Milan P. Stanić
Miles Alan
Mogens Jensen
Nash Kaminski
Natanael Copa
Nathan Rennie-Waldock
Nero
Newbyte
Nia Espera
Nic Boet
Nicolas Lorin
Niklas Meyer
Niko
Nulo
Oleg Titov
Oliver Kuckertz
Oliver Smith
Oliver Wilkes
Olliver Schinagl
Orhun Parmaksız
Pablo Correa Gómez
Paolo Barbolini
Papiris
Paul Spooren
Pedro Lucas Porcellis
Peter Shkenev
Peter van Dijk
Petr Hodina
Philipp Arras
Phillip Jaenke
Piraty
Prokop Randacek
QC8086
R4SAS
Rabindra Dhakal
Ralf Rachinger
Raymond Page
Renê de Souza Pinto
Ricardo F
Rob Blanckaert
Rosie K Languet
Rudolf Polzer
Ruven
SSD
Saijin-Naib
Sam Edwards
Sam Nystrom
Sando
Sascha Brawer
Sean McAvoy
Sergio Talens-Oliag
Sergiy Stupar
Sertonix
Sicelo A. Mhlongo
Simon Frankenberger
Simon Rupf
Simon Zeni
Siva Mahadevan
Sodface
Stanislav Kholmanskikh
Stefan Hansson
Steve McMaster
Steven Brudenell
Steven Guikal
Steven Honson
Steven Vanden Branden
Stuart Cardall
Sylvain Prat
Síle Ekaterin Liszka
Sören Tempel
Ted Trask
Thomas Aldrian
Thomas Böhler
Thomas Deutsch
Thomas Faughnan
Thomas J Faughnan Jr
Thomas Kienlen
Thomas Liske
Tim Magee
Timo Teräs
Timothy Legge
Tom Lebreux
Tom Wieczorek
Umar Getagazov
Usia Bechtle
Val V
Valery Ushakov
Veikka Valtteri Kallinen
Viet Pham
Vlad Glagolev
Vladimir Vitkov
Wesley van Tilburg
Weston Steimel
Will Sinatra
William Desportes
William Walker
Willow Barraco
Yang Xiwen
Yann Vigara
Yingbai He
Zach DeCook
Zachary Andrews
Zsolt Vadasz
adamthiede
alealexpro100
aptalca
bin456789
crapStone
donoban
duckl1ng
elruwen
fanquake
guddaff
hitechshell
j.r
jahway603
jane400
john3dc
jvoisin
knuxify
kpcyrd
lauren n. liberda
lazywalker
leso-kn
lgehr
linear cannon
llightcb
macmpi
mbrowny
messense
michalszmidt
mio
nezu
nibon7
notfound405
omni
ovf
papiris
prspkt
psykose
ptrcnull
qaqland
rubicon
sando38
sodface
streaksu
sudotac
svrnm
takumin
techknowlogick
tetsumaki
tiotags
w
wener
zamfofex
</pre>
