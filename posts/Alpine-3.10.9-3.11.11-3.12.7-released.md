---
title: 'Alpine 3.10.9, 3.11.11 and 3.12.7 released'
date: 2021-04-14
---

Alpine 3.10.9, 3.11.11 and 3.12.7 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.10.9, 3.11.11 and 3.12.7 of its Alpine
Linux operating system.

Those releases include fixes for apk-tools [CVE-2021-30139](https://gitlab.alpinelinux.org/alpine/aports/-/issues/12606).


